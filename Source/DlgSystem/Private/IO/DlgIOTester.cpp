// Copyright 2017-2018 Csaba Molnar, Daniel Butum
#include "IO/DlgIOTester.h"

#include "FileHelper.h"
#include "EnumProperty.h"

#include "IO/DlgConfigWriter.h"
#include "IO/DlgConfigParser.h"
